var satellite = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v10/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9nbml0ZXRlIiwiYSI6ImNqZHQ5N3NyOTBtbDEyd3Qzc21zbGczZGsifQ.fZl8Ld3MTbGPMlG-bB-Cmw', {id: 'satellite-streets-v10'}),
    rue   = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9nbml0ZXRlIiwiYSI6ImNqZHQ5N3NyOTBtbDEyd3Qzc21zbGczZGsifQ.fZl8Ld3MTbGPMlG-bB-Cmw', {id: 'streets-v10'});
    sombre   = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/dark-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9nbml0ZXRlIiwiYSI6ImNqZHQ5N3NyOTBtbDEyd3Qzc21zbGczZGsifQ.fZl8Ld3MTbGPMlG-bB-Cmw', {id: 'dark-v9',});
    defaut = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');


var 

 _firstLatLng,
  _firstPoint,
  _secondLatLng,
  _secondPoint,
  _distance,
  _length,
  _polyline

map = L.map('map',{

       center:[5,28],

       zoom: 7,
       minZoom: 2,
       maxZoom: 18,
       fullscreenControl: true,
			fullscreenControlOptions: { 
				title:"Show me the fullscreen !",
				titleCancel:"Exit fullscreen mode"
			},
       layers:[defaut]

}).setView([-15.106068, 26.472657]);

map.on('enterFullscreen', function(){
			if(window.console) window.console.log('enterFullscreen');
		});
map.on('exitFullscreen', function(){
			if(window.console) window.console.log('exitFullscreen');
		});

var baseMaps = {
    "defaut": defaut,
    "satellite": satellite,
    "rue": rue,
    "sombre":sombre,
};

L.control.layers(baseMaps).addTo(map);


var icone = L.icon({
iconUrl:'hospital.png',
iconSize: [17, 17]
});


function onEachFeature(feature, layer) {
   
    if (feature.properties && feature.properties.NAME1_) {
        layer.bindPopup(feature.properties.NAME1_);
        layer.setIcon(icone);
		
    }
	
	if (layer.feature.properties.name === "Zambia") {
        // Zoom to that layer.
        map.fitBounds(layer.getBounds());
      }
}

L.geoJSON(geo,{
 
     onEachFeature: onEachFeature

}).addTo(map);


$('.pure-button').on('click', function(){
  map.locate({setView: true, maxZoom: 15});
});

map.on('locationfound', onLocationFound);
function onLocationFound(e) {
    console.log(e); 
    L.marker(e.latlng).addTo(map);
}



map.on('click', function(e) {



  if (!_firstLatLng) {
    _firstLatLng = e.latlng;
    _firstPoint = e.layerPoint;
    L.marker(_firstLatLng).addTo(map).bindPopup('Point A<br/>' + e.latlng + '<br/>' + e.layerPoint).openPopup();
  } else {
    _secondLatLng = e.latlng;
    _secondPoint = e.layerPoint;
    L.marker(_secondLatLng).addTo(map).bindPopup('Point B<br/>' + e.latlng + '<br/>' + e.layerPoint).openPopup();
  }

  if (_firstLatLng && _secondLatLng) {
    // draw the line between points
    L.polyline([_firstLatLng, _secondLatLng], {
      color: 'red'
    }).addTo(map);

    refreshDistanceAndLength();
  }
})

map.on('zoomend', function(e) {
  refreshDistanceAndLength();
})

function refreshDistanceAndLength() {
  _distance = L.GeometryUtil.distance(map, _firstLatLng, _secondLatLng);
  _length = L.GeometryUtil.length([_firstPoint, _secondPoint]);
  document.getElementById('distance').innerHTML = _distance;
  document.getElementById('length').innerHTML = _length;
}








